
## 0.0.11 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aviatrix-enable-transit-ha!8

---

## 0.0.10 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/aviatrix-enable-transit-ha!7

---

## 0.0.9 [01-06-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/aviatrix-enable-transit-ha!6

---

## 0.0.8 [10-18-2021]

* [DSUP-903] patch/readme-enable-transit-ha

See merge request itentialopensource/pre-built-automations/aviatrix-enable-transit-ha!3

---

## 0.0.7 [07-07-2021]

* Update README.md, package.json files

See merge request itentialopensource/pre-built-automations/aviatrix-enable-transit-ha!4

---

## 0.0.6 [03-23-2021]

* Patch/dsup 903

See merge request itentialopensource/pre-built-automations/staging/aviatrix-enable-transit-ha!2

---

## 0.0.5 [03-23-2021]

* Patch/dsup 903

See merge request itentialopensource/pre-built-automations/staging/aviatrix-enable-transit-ha!2

---

## 0.0.4 [03-23-2021]

* Patch/dsup 903

See merge request itentialopensource/pre-built-automations/staging/aviatrix-enable-transit-ha!2

---

## 0.0.3 [03-12-2021]

* Patch/dsup 903

See merge request itentialopensource/pre-built-automations/staging/aviatrix-enable-transit-ha!1

---

## 0.0.2 [03-03-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
